# MedicallyBAME Setup

This repository contains a sample CKAN setup for the `ckanext-medicallybame` extension. This repository contains
a copy of the `Dockerfile` from `ckanext-medicallybame`.

## Setup

### Dependencies

- docker
- docker-compose

### Environment Variables

The `.env.sample` file lists all environment variables required for a successful build and execution of the
MedicallyBAME data portal. Create a copy of `.env.sample` named `.env` and place it within the project root
directory. This is the same directory that has the `docker-compose.yml` file. Update the contents of `.env`
to assign a value to all defined variables.

#### What is GITHUB_TOKEN?

The setup needs to access the `ckanext-medicallybame` repository on GitHub. Since the repository is private, an
access token is required. See the official GitHub documentation on creating such a token [here](
https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token).

## Running Setup

Execute the following command to get the setup running:

```bash
# build images
docker-compose build

# run setup
docker-compose up
```
