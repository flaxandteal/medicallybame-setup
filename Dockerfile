FROM hazeltek/ckan:2.8.4-alpine

LABEL maintainer="Abdul-Hakeem Shaibu <s.abdulhakeeem@gmail.com>"

ARG GITHUB_TOKEN
ENV CKAN_SITE_URL http://ckan:5000

USER root
RUN apk add --no-cache \
            g++ \
            gcc \
            musl-dev \
            python2-dev

RUN pip install --upgrade pip &&\
    pip install -e git+https://${GITHUB_TOKEN}@github.com/dkayee/ckanext-medicallybame.git#egg=ckanext-medicallybame &&\
    pip install -r https://${GITHUB_TOKEN}@raw.githubusercontent.com/dkayee/ckanext-medicallybame/master/requirements.txt

ENV CKAN__PLUGINS envvars medicallybame datastore datapusher stats image_view \
                  recline_view text_view webpage_view

RUN mkdir -p /var/lib/ckan && chown -R ckan:ckan /var/lib/ckan
VOLUME /var/lib/ckan/

# Load envvars plugins on ini file
RUN paster --plugin=ckan config-tool ${APP_DIR}/production.ini "ckan.plugins = ${CKAN__PLUGINS}"
RUN paster --plugin=ckan config-tool ${APP_DIR}/production.ini "ckan.site_url = ${CKAN_SITE_URL}"


USER ckan

CMD ["/srv/app/start_ckan.sh"]
