.PHONY: clean up-ckan up-core

clean:
	rm -rf ./.data

up-core:
	docker-compose up postgres solr redis

up-ckan:
	docker-compose up ckan datapusher
